%---------------------- Instances details ----------------------%

- S: group of instances with small (S) dimensions
- M_250 & M_25: group of instances with medium (M) dimensions
- L: group of instances with large (L) dimensions


%----------------------  Instances format ----------------------%

The instances are in the following format:

Piece   Reference   Width   Length   Demand

Fabric_roll   Reference   Width_j

where, forall i=1,...,I: 
- Piece: piece index (i)
- Reference: piece reference (r)  
- Width: width of item type i (w_i)   
- Length: length of item type i (l_i)   
- Demand: demand of item type i (d_i)

, and forall j=1,...,J:
- Fabric_roll: fabric roll index (j)
- Reference: fabric roll reference (r)    
- Width_j: Width of plate type j (W_j)


